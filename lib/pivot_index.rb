# frozen_string_literal: true

module PivotIndex
  NOT_FOUND = -1

  module_function

  def find(array)
    return NOT_FOUND if array.length < 2
    total = array.sum
    temporary = 0
    array.each_with_index do |number, index|
      total -= number
      return index if temporary == total
      temporary += number
    end
    return NOT_FOUND
  end
end
