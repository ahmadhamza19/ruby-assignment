# frozen_string_literal: true

require './lib/pivot_index.rb'

RSpec.describe PivotIndex do
  subject(:call) do
    described_class.find(integers)
  end

  context 'success' do
    context 'only positive numbers' do
      let(:integers) { [1, 4, 6, 3, 2] }

      it 'returns pivot index' do
        expect(call).to eq(2)
      end
    end

    context 'only negative numbers' do
      let(:integers) { [-1, -4, -6, -3, -2] }

      it 'returns pivot index' do
        expect(call).to eq(2)
      end
    end

    context 'multiple pivot indexes' do
      let(:integers) { [1, 1, 1, 1, 1] }

      it 'returns pivot index' do
        expect(call).to eq(2)
      end
    end
  end

  shared_examples 'returns -1' do
    it 'does not find any index' do
      expect(call).to eq(-1)
    end
  end

  context 'no pivot index' do
    let(:integers) { [1, -4, 6, 3, 2] }

    it_behaves_like 'returns -1'
  end

  context 'array of 3 integers' do
    let(:integers) { [1, 2, 3] }

    it_behaves_like 'returns -1'
  end

  context 'array of single element' do
    let(:integers) { [1] }

    it_behaves_like 'returns -1'
  end
end
